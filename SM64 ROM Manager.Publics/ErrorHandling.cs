﻿using Assimp;
using DevComponents.DotNetBar;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SM64_ROM_Manager.Publics
{
    public static class ErrorHandling
    {

        public static void HandleUnhandledException(Microsoft.VisualBasic.ApplicationServices.UnhandledExceptionEventArgs e)
        {
            if (!Debugger.IsAttached)
            {
                if (e.Exception is AssimpException)
                {
                    MessageBoxEx.Show(string.Format(My.Resources.ErrorDialogLangRes.MsgBox_AssimpException, e.Exception.Message), My.Resources.ErrorDialogLangRes.MsgBox_AssimpException_Title, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.ExitApplication = false;
                }
                else
                {
                    var frm = new Form_ErrorDialog();
                    frm.CurrentException = e.Exception;
                    frm.ShowDialog();
                    e.ExitApplication = frm.ExitApplicaiton;
                    frm.Dispose();
                }
            }
        }

    }
}
