﻿using SM64_ROM_Manager.UserRequests;
using SM64_ROM_Manager.UserRequests.GUI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM64_ROM_Manager.Publics
{
    public class UserRequestHelper
    {
        public static void OpenFeatureRequestDialog(bool online)
        {
            OpenUserRequestDialog(online, Path.Combine(General.MyUserRequestsPath, "FeatureRequest.json"), WebLinks.FeatureRequestForm, WebLinks.FeatureRequestFormInternal);
        }

        public static void OpenBugReportDialog(bool online)
        {
            OpenUserRequestDialog(online, Path.Combine(General.MyUserRequestsPath, "BugReport.json"), WebLinks.BugReportForm, WebLinks.BugReportFormInternal);
        }

        public static void OpenTranslationSubmition(bool online)
        {
            OpenUserRequestDialog(online, Path.Combine(General.MyUserRequestsPath, "SubmitTranslation.json"), WebLinks.TranslationSubmitionForm, WebLinks.TranslationSubmitionFormInternal);
        }

        private static void OpenUserRequestDialog(bool online, string requestFilePath, string onlineUrl, string internalUrl)
        {
            if (online)
                Publics.OpenBrowser(new Uri(onlineUrl), new Uri(internalUrl), true, new Size(1200, 700));
            else
            {
                var frm = new UserRequestDialog(UserRequestLayout.LoadFrom(requestFilePath));
                frm.Show();
            }
        }
    }
}
