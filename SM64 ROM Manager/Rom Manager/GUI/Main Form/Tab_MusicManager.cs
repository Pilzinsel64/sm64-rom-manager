﻿using System;
using System.Runtime.CompilerServices;
using global::DevComponents.DotNetBar;
using Microsoft.VisualBasic.CompilerServices;
using global::SM64_ROM_Manager.EventArguments;
using SM64Lib.TextValueConverter;
using DevComponents.AdvTree;
using DevComponents.DotNetBar.Controls;
using System.Windows.Forms;
using System.Globalization;
using System.Collections;

namespace SM64_ROM_Manager
{
    public partial class Tab_MusicManager
    {
        // F i e l d s

        private MainController _Controller;
        private bool settingSequenceName = false;

        // C o n s t r u c t o r

        public Tab_MusicManager()
        {
            InitializeComponent();
        }

        // P r o p e r t i e s

        public MainController Controller
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _Controller;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_Controller != null)
                {

                    // C o n t r o l l e r   E v e n t s

                    _Controller.MusicSequenceRemoved -= Controller_MusicSequenceRemoved;
                    _Controller.MusicSequenceChanged -= Controller_MusicSequenceChanged;
                    _Controller.MusicSequenceAdded -= Controller_MusicSequenceAdded;
                    _Controller.RomMusicLoaded -= Controller_RomMusicLoaded;
                }

                _Controller = value;
                if (_Controller != null)
                {
                    _Controller.MusicSequenceRemoved += Controller_MusicSequenceRemoved;
                    _Controller.MusicSequenceChanged += Controller_MusicSequenceChanged;
                    _Controller.MusicSequenceAdded += Controller_MusicSequenceAdded;
                    _Controller.RomMusicLoaded += Controller_RomMusicLoaded;
                }
            }
        }

        // P r o p e r t i e s

        private int SelectedSequenceIndex => AdvTree_Sequences.SelectedIndex;

        private void Controller_MusicSequenceRemoved(MusicSequenceEventArgs e)
        {
            RemoveSequenceFromList(e.Index);
        }

        private void Controller_MusicSequenceChanged(MusicSequenceEventArgs e)
        {
            UpdateSequenceInList(e.Index);
            if (e.Index == SelectedSequenceIndex && !settingSequenceName)
                LoadCurrentSequence(e.Index);
        }

        private void Controller_MusicSequenceAdded(MusicSequenceEventArgs e)
        {
            AddSequenceToList(e.Index, true);
        }

        private void Controller_RomMusicLoaded()
        {
            LoadSequenceList();
        }

        // G u i

        private void LoadSequenceList()
        {
            AdvTree_Sequences.BeginUpdate();

            // Remember old Index
            int IndexBefore = SelectedSequenceIndex;

            // Clear Items
            AdvTree_Sequences.Nodes.Clear();

            // Add new items
            for (int i = 0, loopTo = Controller.GetMusicSeuenceCount() - 1; i <= loopTo; i++)
                AddSequenceToList(i, false);
            AdvTree_Sequences.EndUpdate();
            buttonItem_EnableOverwriteSizeRestricitons.Checked = Controller.EnableMusicHack;
            
            if (IndexBefore < 0)
                IndexBefore = 1;
            if (AdvTree_Sequences.Nodes.Count > IndexBefore)
                AdvTree_Sequences.SelectedIndex = IndexBefore;
            else if (AdvTree_Sequences.Nodes.Count > 1)
                AdvTree_Sequences.SelectedIndex = 1;
        }

        private void RemoveSequenceFromList(int index)
        {
            AdvTree_Sequences.Nodes.RemoveAt(index);
            
            if (index > 0)
                AdvTree_Sequences.SelectedIndex = index - 1;

            foreach (Node n in AdvTree_Sequences.Nodes)
                UpdateSequenceInList(n.Index, n);
        }

        private void LoadCurrentSequence(int index)
        {
            int cIndex = index;
            bool IsNoIndex = cIndex < 0;
            bool IsIndex0 = cIndex == 0;

            buttonItem_EditSequenceInHexEditor.Enabled = !IsIndex0;
            buttonItem_RemoveSequence.Enabled = !IsIndex0;
        }

        private void AddSequenceToList(int index, bool refreshAndSelect)
        {
            var btnItem = new Node
            {
                ContextMenu = buttonItem_CM_Sequence,
                Editable = index != 0
            };

            btnItem.Cells[0].Name = "ID";
            btnItem.Cells.Add(new Cell { Name = "Name" });
            btnItem.Cells.Add(new Cell { Name = "NInst" });
            btnItem.Cells.Add(new Cell { Name = "Size" });

            UpdateSequenceInList(index, btnItem);

            AdvTree_Sequences.Nodes.Insert(index, btnItem);

            if (refreshAndSelect)
            {
                AdvTree_Sequences.Refresh();
                btnItem.EnsureVisible();
            }
        }

        private void UpdateSequenceInList(int index)
        {
            UpdateSequenceInList(index, AdvTree_Sequences.Nodes[index]);
            AdvTree_Sequences.Refresh();
        }

        private void UpdateSequenceInList(int index, Node buttonItem)
        {
            var infos = Controller.GetMusicSequenceInfos(index);
            string tName = infos.name;
            string tSeqID = string.Format("{0}", TextValueConverter.TextFromValue(index, charCount: 2));
            string tSeqLength = string.Format("{0} Bytes", TextValueConverter.TextFromValue(infos.length));
            string tNInst = My.Resources.NInstNamesDictionary.ResourceManager.GetString("NInst" + infos.instSet.ToString("00"));
            buttonItem.Cells["ID"].Text = tSeqID;
            buttonItem.Cells["Name"].Text = tName;
            buttonItem.Cells["NInst"].Text = tNInst;
            buttonItem.Cells["Size"].Text = tSeqLength;
        }

        private void SwitchButton_MS_OverwriteSizeRestrictions_ValueChanged(object sender, EventArgs e)
        {
            Controller.EnableMusicHack = buttonItem_EnableOverwriteSizeRestricitons.Checked;
        }

        private void Button_MS_ReplaceSequence_Click(object sender, EventArgs e)
        {
            Controller.ReplaceMusicSequence(SelectedSequenceIndex);
        }

        private void ButtonX_MS_AddSequence_Click(object sender, EventArgs e)
        {
            Controller.AddNewMusicSequence();
        }

        private void Button_MS_ExtractSequence_Click(object sender, EventArgs e)
        {
            Controller.ExtractMusicSequence(SelectedSequenceIndex);
        }

        private void ButtonX_MS_RemoveSequence_Click(object sender, EventArgs e)
        {
            Controller.RemoveMusicSequence(SelectedSequenceIndex);
        }

        private void ButtonX1_Click(object sender, EventArgs e)
        {
            Controller.EditMusicSequenceInHexEditor(SelectedSequenceIndex);
        }

        private void AdvTree_Sequences_ProvideCustomCellEditor(object sender, CustomCellEditorEventArgs e)
        {
            switch (e.Cell.Name)
            {
                case "NInst":
                    e.EditControl = new NInstEditor();
                    break;
            }
        }

        private void AdvTree_Sequences_PrepareCellEditorControl(object sender, PrepareCellEditorEventArgs e)
        {
        }

        private void AdvTree_Sequences_AfterCellEdit(object sender, CellEditEventArgs e)
        {
            switch (e.Cell.Name)
            {
                case "Name":
                    settingSequenceName = true;
                    Controller.SetMusicSequenceName(SelectedSequenceIndex, e.NewText);
                    settingSequenceName = false;
                    break;
                case "NInst":
                    {
                        var theID = (byte)0;
                        var found = false;
                        var resSet = My.Resources.NInstNamesDictionary.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);

                        foreach (DictionaryEntry kvp in resSet)
                        {
                            if (!found && kvp.Value.Equals(e.NewText))
                            {
                                found = true;
                                theID = Convert.ToByte(((string)kvp.Key).Substring(5));
                            }
                        }

                        Controller.SetMusicSequenceInstrumentSet(SelectedSequenceIndex, theID);
                    }
                    break;
            }
        }

        private void AdvTree_Sequences_BeforeCellEdit(object sender, CellEditEventArgs e)
        {
        }
    }

    #region Integer Custom Editor
    internal class NInstEditor : ComboBoxEx, ICellEditControl
    {
        #region ICellEditControl Members

        bool settingValue = false;

        public NInstEditor()
        {
            DrawMode = DrawMode.OwnerDrawFixed;
            DropDownStyle = ComboBoxStyle.DropDownList;
            FormattingEnabled = true;
            Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            DropDownHeight = 150;
            AddNInstItems();
            Sorted = true;
            StyleManager.UpdateAmbientColors(this);
        }

        private void AddNInstItems()
        {
            var set = My.Resources.NInstNamesDictionary.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            foreach (DictionaryEntry kvp in set)
                Items.Add(kvp.Value);
        }

        public void BeginEdit()
        {
            this.MinimumSize = new System.Drawing.Size(32, 10);
            DroppedDown = true;
        }

        public void EndEdit()
        {
            this.Dispose();
        }

        public object CurrentValue
        {
            get
            {
                return this.SelectedItem;
            }
            set
            {
                settingValue = true;
                this.SelectedItem = value;
                settingValue = false;
            }
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter || keyData == Keys.Tab)
            {
                if (EditComplete != null)
                    EditComplete(this, new EventArgs());
                return true;
            }
            else if (keyData == Keys.Escape)
            {
                if (CancelEdit != null)
                    CancelEdit(this, new EventArgs());
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        public event EventHandler EditComplete;

        public event EventHandler CancelEdit;

        public bool EditWordWrap
        {
            get { return false; }
            set { }
        }

        protected override void OnSelectedValueChanged(EventArgs e)
        {
            base.OnSelectedValueChanged(e);
            if (!settingValue)
                EditComplete?.Invoke(this, new EventArgs());
        }

        protected override void OnDropDownClosed(EventArgs e)
        {
            base.OnDropDownClosed(e);
            if (!settingValue)
                EditComplete?.Invoke(this, new EventArgs());
        }

        #endregion
    }
    #endregion
}