﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace SM64_ROM_Manager
{
    [DesignerGenerated()]
    public partial class Tab_MusicManager : UserControl
    {

        // UserControl überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components is object)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Wird vom Windows Form-Designer benötigt.
        private System.ComponentModel.IContainer components;

        // Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
        // Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
        // Das Bearbeiten mit dem Code-Editor ist nicht möglich.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Tab_MusicManager));
            this.AdvTree_Sequences = new DevComponents.AdvTree.AdvTree();
            this.columnHeader_ID = new DevComponents.AdvTree.ColumnHeader();
            this.columnHeader_Name = new DevComponents.AdvTree.ColumnHeader();
            this.columnHeader_NInst = new DevComponents.AdvTree.ColumnHeader();
            this.columnHeader_Size = new DevComponents.AdvTree.ColumnHeader();
            this.nodeConnector1 = new DevComponents.AdvTree.NodeConnector();
            this.elementStyle1 = new DevComponents.DotNetBar.ElementStyle();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.ButtonItem_AddSequence = new DevComponents.DotNetBar.ButtonItem();
            this.ButtonItem_ImportSequence = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem_Options = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem_EnableOverwriteSizeRestricitons = new DevComponents.DotNetBar.ButtonItem();
            this.contextMenuBar1 = new DevComponents.DotNetBar.ContextMenuBar();
            this.buttonItem_CM_Sequence = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem_ExportSequence = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem_EditSequenceInHexEditor = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem_RemoveSequence = new DevComponents.DotNetBar.ButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.AdvTree_Sequences)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contextMenuBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // AdvTree_Sequences
            // 
            this.AdvTree_Sequences.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline;
            this.AdvTree_Sequences.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.AdvTree_Sequences.BackgroundStyle.Class = "TreeBorderKey";
            this.AdvTree_Sequences.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.AdvTree_Sequences.CellEdit = true;
            this.AdvTree_Sequences.Columns.Add(this.columnHeader_ID);
            this.AdvTree_Sequences.Columns.Add(this.columnHeader_Name);
            this.AdvTree_Sequences.Columns.Add(this.columnHeader_NInst);
            this.AdvTree_Sequences.Columns.Add(this.columnHeader_Size);
            resources.ApplyResources(this.AdvTree_Sequences, "AdvTree_Sequences");
            this.AdvTree_Sequences.DragDropEnabled = false;
            this.AdvTree_Sequences.ExpandWidth = 0;
            this.AdvTree_Sequences.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.AdvTree_Sequences.Name = "AdvTree_Sequences";
            this.AdvTree_Sequences.NodesConnector = this.nodeConnector1;
            this.AdvTree_Sequences.NodeStyle = this.elementStyle1;
            this.AdvTree_Sequences.PathSeparator = ";";
            this.AdvTree_Sequences.Styles.Add(this.elementStyle1);
            this.AdvTree_Sequences.PrepareCellEditorControl += new DevComponents.AdvTree.PrepareCellEditorEventHandler(this.AdvTree_Sequences_PrepareCellEditorControl);
            this.AdvTree_Sequences.BeforeCellEdit += new DevComponents.AdvTree.CellEditEventHandler(this.AdvTree_Sequences_BeforeCellEdit);
            this.AdvTree_Sequences.AfterCellEdit += new DevComponents.AdvTree.CellEditEventHandler(this.AdvTree_Sequences_AfterCellEdit);
            this.AdvTree_Sequences.ProvideCustomCellEditor += new DevComponents.AdvTree.CustomCellEditorEventHandler(this.AdvTree_Sequences_ProvideCustomCellEditor);
            // 
            // columnHeader_ID
            // 
            this.columnHeader_ID.Editable = false;
            this.columnHeader_ID.Name = "columnHeader_ID";
            resources.ApplyResources(this.columnHeader_ID, "columnHeader_ID");
            this.columnHeader_ID.Width.Absolute = 40;
            // 
            // columnHeader_Name
            // 
            this.columnHeader_Name.Name = "columnHeader_Name";
            resources.ApplyResources(this.columnHeader_Name, "columnHeader_Name");
            this.columnHeader_Name.Width.Absolute = 250;
            // 
            // columnHeader_NInst
            // 
            this.columnHeader_NInst.EditorType = DevComponents.AdvTree.eCellEditorType.Custom;
            this.columnHeader_NInst.Name = "columnHeader_NInst";
            resources.ApplyResources(this.columnHeader_NInst, "columnHeader_NInst");
            this.columnHeader_NInst.Width.Absolute = 200;
            // 
            // columnHeader_Size
            // 
            this.columnHeader_Size.Editable = false;
            this.columnHeader_Size.Name = "columnHeader_Size";
            resources.ApplyResources(this.columnHeader_Size, "columnHeader_Size");
            this.columnHeader_Size.Width.Absolute = 100;
            // 
            // nodeConnector1
            // 
            this.nodeConnector1.LineColor = System.Drawing.SystemColors.ControlText;
            // 
            // elementStyle1
            // 
            this.elementStyle1.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.elementStyle1.Name = "elementStyle1";
            this.elementStyle1.TextColor = System.Drawing.SystemColors.ControlText;
            // 
            // bar1
            // 
            this.bar1.AntiAlias = true;
            resources.ApplyResources(this.bar1, "bar1");
            this.bar1.IsMaximized = false;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ButtonItem_AddSequence,
            this.ButtonItem_ImportSequence,
            this.buttonItem_Options});
            this.bar1.Name = "bar1";
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabStop = false;
            // 
            // ButtonItem_AddSequence
            // 
            this.ButtonItem_AddSequence.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.ButtonItem_AddSequence.Image = global::SM64_ROM_Manager.My.Resources.MyIcons.icons8_plus_math_16px;
            this.ButtonItem_AddSequence.Name = "ButtonItem_AddSequence";
            resources.ApplyResources(this.ButtonItem_AddSequence, "ButtonItem_AddSequence");
            this.ButtonItem_AddSequence.Click += new System.EventHandler(this.ButtonX_MS_AddSequence_Click);
            // 
            // ButtonItem_ImportSequence
            // 
            this.ButtonItem_ImportSequence.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.ButtonItem_ImportSequence.Image = global::SM64_ROM_Manager.My.Resources.MyIcons.icons8_import_16px;
            this.ButtonItem_ImportSequence.Name = "ButtonItem_ImportSequence";
            resources.ApplyResources(this.ButtonItem_ImportSequence, "ButtonItem_ImportSequence");
            this.ButtonItem_ImportSequence.Click += new System.EventHandler(this.Button_MS_ReplaceSequence_Click);
            // 
            // buttonItem_Options
            // 
            this.buttonItem_Options.AutoExpandOnClick = true;
            this.buttonItem_Options.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem_Options.Image = global::SM64_ROM_Manager.My.Resources.MyIcons.icons8_settings_16px;
            this.buttonItem_Options.Name = "buttonItem_Options";
            this.buttonItem_Options.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem_EnableOverwriteSizeRestricitons});
            resources.ApplyResources(this.buttonItem_Options, "buttonItem_Options");
            // 
            // buttonItem_EnableOverwriteSizeRestricitons
            // 
            this.buttonItem_EnableOverwriteSizeRestricitons.AutoCheckOnClick = true;
            this.buttonItem_EnableOverwriteSizeRestricitons.Name = "buttonItem_EnableOverwriteSizeRestricitons";
            resources.ApplyResources(this.buttonItem_EnableOverwriteSizeRestricitons, "buttonItem_EnableOverwriteSizeRestricitons");
            this.buttonItem_EnableOverwriteSizeRestricitons.CheckedChanged += new System.EventHandler(this.SwitchButton_MS_OverwriteSizeRestrictions_ValueChanged);
            // 
            // contextMenuBar1
            // 
            this.contextMenuBar1.AntiAlias = true;
            resources.ApplyResources(this.contextMenuBar1, "contextMenuBar1");
            this.contextMenuBar1.IsMaximized = false;
            this.contextMenuBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem_CM_Sequence});
            this.contextMenuBar1.Name = "contextMenuBar1";
            this.contextMenuBar1.Stretch = true;
            this.contextMenuBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.contextMenuBar1.TabStop = false;
            // 
            // buttonItem_CM_Sequence
            // 
            this.buttonItem_CM_Sequence.AutoExpandOnClick = true;
            this.buttonItem_CM_Sequence.Name = "buttonItem_CM_Sequence";
            this.buttonItem_CM_Sequence.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem_ExportSequence,
            this.buttonItem_EditSequenceInHexEditor,
            this.buttonItem_RemoveSequence});
            resources.ApplyResources(this.buttonItem_CM_Sequence, "buttonItem_CM_Sequence");
            // 
            // buttonItem_ExportSequence
            // 
            this.buttonItem_ExportSequence.Image = global::SM64_ROM_Manager.My.Resources.MyIcons.icons8_export_16px;
            this.buttonItem_ExportSequence.Name = "buttonItem_ExportSequence";
            resources.ApplyResources(this.buttonItem_ExportSequence, "buttonItem_ExportSequence");
            this.buttonItem_ExportSequence.Click += new System.EventHandler(this.Button_MS_ExtractSequence_Click);
            // 
            // buttonItem_EditSequenceInHexEditor
            // 
            this.buttonItem_EditSequenceInHexEditor.Image = global::SM64_ROM_Manager.My.Resources.MyIcons.icons8_edit_16px;
            this.buttonItem_EditSequenceInHexEditor.Name = "buttonItem_EditSequenceInHexEditor";
            resources.ApplyResources(this.buttonItem_EditSequenceInHexEditor, "buttonItem_EditSequenceInHexEditor");
            this.buttonItem_EditSequenceInHexEditor.Click += new System.EventHandler(this.ButtonX1_Click);
            // 
            // buttonItem_RemoveSequence
            // 
            this.buttonItem_RemoveSequence.Image = global::SM64_ROM_Manager.My.Resources.MyIcons.icons8_delete_sign_16px;
            this.buttonItem_RemoveSequence.Name = "buttonItem_RemoveSequence";
            resources.ApplyResources(this.buttonItem_RemoveSequence, "buttonItem_RemoveSequence");
            this.buttonItem_RemoveSequence.Click += new System.EventHandler(this.ButtonX_MS_RemoveSequence_Click);
            // 
            // Tab_MusicManager
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.contextMenuBar1);
            this.Controls.Add(this.AdvTree_Sequences);
            this.Controls.Add(this.bar1);
            this.Name = "Tab_MusicManager";
            ((System.ComponentModel.ISupportInitialize)(this.AdvTree_Sequences)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contextMenuBar1)).EndInit();
            this.ResumeLayout(false);

        }

        private DevComponents.AdvTree.AdvTree AdvTree_Sequences;
        private DevComponents.AdvTree.NodeConnector nodeConnector1;
        private DevComponents.DotNetBar.ElementStyle elementStyle1;
        private DevComponents.AdvTree.ColumnHeader columnHeader_ID;
        private DevComponents.AdvTree.ColumnHeader columnHeader_Name;
        private DevComponents.AdvTree.ColumnHeader columnHeader_NInst;
        private DevComponents.AdvTree.ColumnHeader columnHeader_Size;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.ButtonItem ButtonItem_AddSequence;
        private DevComponents.DotNetBar.ButtonItem ButtonItem_ImportSequence;
        private DevComponents.DotNetBar.ButtonItem buttonItem_Options;
        private DevComponents.DotNetBar.ButtonItem buttonItem_EnableOverwriteSizeRestricitons;
        private DevComponents.DotNetBar.ContextMenuBar contextMenuBar1;
        private DevComponents.DotNetBar.ButtonItem buttonItem_CM_Sequence;
        private DevComponents.DotNetBar.ButtonItem buttonItem_ExportSequence;
        private DevComponents.DotNetBar.ButtonItem buttonItem_EditSequenceInHexEditor;
        private DevComponents.DotNetBar.ButtonItem buttonItem_RemoveSequence;

    }
}