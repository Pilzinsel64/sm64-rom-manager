﻿using DevComponents.AdvTree;
using DevComponents.DotNetBar;
using SM64_ROM_Manager.Publics;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using WebDav;

namespace SM64_ROM_Manager
{
    public partial class Form_Changelog : OfficeForm
    {
        private WebDavClient webDavClient;

        public Form_Changelog()
        {
            InitializeComponent();
        }

        private async Task DownloadAndParse()
        {
            var wc = new WebClient();
            var markdown = await wc.DownloadStringTaskAsync(new Uri(WebLinks.ChangelogDownload));
            var html = Markdig.Markdown.ToHtml(markdown);
            html = $"<p style=\"color:#{ForeColor.ToArgb().ToString("X8").Substring(2)};\">{html}</p>";
            htmlPanel1.Text = html;

            // Maybe somewhen in the feature for listing each version and show only the content of the selected version?
            // if (Regex.IsMatch(fileName, @"Version (\d+)\.(\d+)(\.(\d+))?(\.(\d+))?\.*"))
            //     New version found! Store in KeyValuePair-List
        }

        private async void Form_Changelog_Shown(object sender, EventArgs e)
        {
            cirProgTree.Start();
            await DownloadAndParse();
            cirProgTree.Stop();
        }
    }
}
