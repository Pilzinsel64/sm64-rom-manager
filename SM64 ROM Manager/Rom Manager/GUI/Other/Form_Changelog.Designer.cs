﻿
namespace SM64_ROM_Manager
{
    partial class Form_Changelog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Changelog));
            this.cirProgTree = new DevComponents.DotNetBar.Controls.CircularProgress();
            this.htmlPanel1 = new TheArtOfDev.HtmlRenderer.WinForms.HtmlPanel();
            this.cirProgView = new DevComponents.DotNetBar.Controls.CircularProgress();
            this.htmlPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cirProgTree
            // 
            resources.ApplyResources(this.cirProgTree, "cirProgTree");
            // 
            // 
            // 
            this.cirProgTree.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.cirProgTree.Name = "cirProgTree";
            this.cirProgTree.ProgressBarType = DevComponents.DotNetBar.eCircularProgressType.Dot;
            this.cirProgTree.SetVisibleStateOnStart = true;
            this.cirProgTree.SetVisibleStateOnStop = true;
            this.cirProgTree.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            // 
            // htmlPanel1
            // 
            resources.ApplyResources(this.htmlPanel1, "htmlPanel1");
            this.htmlPanel1.BackColor = System.Drawing.SystemColors.Window;
            this.htmlPanel1.BaseStylesheet = null;
            this.htmlPanel1.Controls.Add(this.cirProgView);
            this.htmlPanel1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.htmlPanel1.Name = "htmlPanel1";
            // 
            // cirProgView
            // 
            resources.ApplyResources(this.cirProgView, "cirProgView");
            // 
            // 
            // 
            this.cirProgView.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.cirProgView.Name = "cirProgView";
            this.cirProgView.ProgressBarType = DevComponents.DotNetBar.eCircularProgressType.Dot;
            this.cirProgView.SetVisibleStateOnStart = true;
            this.cirProgView.SetVisibleStateOnStop = true;
            this.cirProgView.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            // 
            // Form_Changelog
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.htmlPanel1);
            this.Name = "Form_Changelog";
            this.Shown += new System.EventHandler(this.Form_Changelog_Shown);
            this.htmlPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private TheArtOfDev.HtmlRenderer.WinForms.HtmlPanel htmlPanel1;
        private DevComponents.DotNetBar.Controls.CircularProgress cirProgView;
        private DevComponents.DotNetBar.Controls.CircularProgress cirProgTree;
    }
}