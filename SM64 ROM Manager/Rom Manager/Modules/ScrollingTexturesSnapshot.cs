﻿using SM64Lib.Configuration;
using SM64Lib.Levels;
using SM64Lib.Levels.ScrolTex;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM64_ROM_Manager
{
    class ScrollingTexturesSnapshot
    {
        private List<ManagedScrollingTexture> OldScrollTexs { get; } = new();
        private Dictionary<short, string> OldScrollTexNames { get; } = new();

        private ScrollingTexturesSnapshot()
        {
        }

        public static ScrollingTexturesSnapshot GetSnapshot(LevelArea area, LevelAreaConfig areaConfig)
        {
            var scrollTexSnap = new ScrollingTexturesSnapshot();

            // Remember managed scrolling textures
            scrollTexSnap.OldScrollTexs.AddRange(area.ScrollingTextures);

            // Remember scrolling texture names
            foreach (var kvp in areaConfig.ScrollingNames)
                scrollTexSnap.OldScrollTexNames.Add(kvp.Key, kvp.Value);

            return scrollTexSnap;
        }

        public void PasteSnaphot(LevelArea area, LevelAreaConfig areaConfig)
        {
            foreach (var stname in areaConfig.ScrollingNames.Values)
            {
                bool checkST(ManagedScrollingTexture st, Dictionary<short, string> scrollTexNames)
                {
                    string stn;
                    if (!scrollTexNames.TryGetValue(st.GroupID, out stn))
                        stn = string.Empty;
                    return stn == stname;
                }

                var oldSTs = OldScrollTexs.Where(n => checkST(n, OldScrollTexNames));
                var newSTs = area.ScrollingTextures.Where(n => checkST(n, areaConfig.ScrollingNames));
                var minCount = Math.Min(oldSTs.Count(), newSTs.Count());

                for (int i = 0; i < minCount; i++)
                {
                    var oldST = oldSTs.ElementAt(i);
                    var newST = newSTs.ElementAt(i);
                    oldST.CopyPropsTo(newST);
                }
            }
        }
    }
}
