﻿using Pilz.Drawing.Drawing3D.OpenGLFactory.CameraN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM64_ROM_Manager.SettingsManager
{
    public class RomSettingsStruc
    {
        public bool EnableNewRomMD5Check { get; set; }
        public bool EnableNewRomSM64EditorCheck { get; set; }
        public bool EnableNewRomFileSizeCheck { get; set; }

        public void ResetValues()
        {
            EnableNewRomMD5Check = true;
            EnableNewRomSM64EditorCheck = true;
            EnableNewRomFileSizeCheck = true;
        }
    }
}
