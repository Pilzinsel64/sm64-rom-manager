﻿using global::Pilz.Configuration;

namespace SM64_ROM_Manager.SettingsManager
{
    public class SettingsStruc : SettingsBase
    {
        public AreaEditorSettingsStruc AreaEditor { get; set; }
        public GeneralSettingsStruc General { get; set; }
        public StyleManagerSettingsStruc StyleManager { get; set; }
        public FileParserSettingsStruc FileParser { get; set; }
        public RecentFilesSettingsStruc RecentFiles { get; set; }
        public ModelConverterSettingsStruc ModelConverter { get; set; }
        public JobsToDoStruc JobsToDo { get; set; }
        public TextManagerSettingsStruc TextManager { get; set; }
        public NetworkSettingsStruc Network { get; set; }
        public WindowSettingsStruc WindowSettings { get; set; }
        public LevelManagerSettingsStruc LevelManager { get; set; }
        public RomSettingsStruc RomSettings { get; set; }
        public DiagnosticDataStruc DiagnosticData { get; set; }

        public override void ResetValues()
        {
            AreaEditor ??= new AreaEditorSettingsStruc();
            AreaEditor.ResetValues();

            General ??= new GeneralSettingsStruc();
            General.ResetValues();

            StyleManager ??= new StyleManagerSettingsStruc();
            StyleManager.ResetValues();

            FileParser ??= new FileParserSettingsStruc();
            FileParser.ResetValues();

            RecentFiles ??= new RecentFilesSettingsStruc();
            RecentFiles.ResetValues();

            ModelConverter ??= new ModelConverterSettingsStruc();
            ModelConverter.ResetValues();

            JobsToDo ??= new JobsToDoStruc();
            JobsToDo.Clear();

            TextManager ??= new TextManagerSettingsStruc();
            TextManager.ResetValues();

            Network ??= new NetworkSettingsStruc();
            Network.ResetValues();

            WindowSettings ??= new WindowSettingsStruc();
            WindowSettings.ResetValues();

            LevelManager ??= new LevelManagerSettingsStruc();
            LevelManager.ResetValues();

            RomSettings ??= new RomSettingsStruc();
            RomSettings.ResetValues();

            DiagnosticData ??= new DiagnosticDataStruc();
            DiagnosticData.ResetValues();
        }
    }
}