﻿using global::SM64Lib.Geolayout;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;

namespace SM64Lib.Model.Conversion.Fast3DWriting
{
    public class DisplaylistSelectionSettings
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public DisplaylistSelectionMode SelectionMode { get; set; } = DisplaylistSelectionMode.Automatic;
        [JsonConverter(typeof(StringEnumConverter))]
        public DefaultGeolayers DefaultGeolayer { get; set; } = DefaultGeolayers.Solid;
        public int CustomDisplaylistID { get; set; } = 0;
    }
}