﻿namespace SM64Lib.Objects
{
    public enum StarPositionAddress
    {
        KoopaTheQuick1 = 0xED868,
        KoopaTheQuick2 = 0xED878,
        KingBobOmbBoss = 0x1204F00,
        WhompBoss = 0x1204F10,
        EyerockBoss = 0x1204F20,
        BigBullyBoss = 0x1204F30,
        ChillBullyBoss = 0x1204F40,
        GiantPiranhaPlants = 0x1204F50,
        PenguinMother = 0x1204F60,
        BigPenguinRace = 0x1204F90,
        WigglerBoss = 0x1204F70,
        PeachSlideStar = 0x1204F80,
        TreasureChests = 0x1204FA0,
        BooInHauntedHouse = 0x1204FAC,
        Klepto = 0x1204FC4,
        MerryGoRoundboss = 0x1204FB8,
        MrIboss = 0x1204FD0,
        RooftopBoo = 0x1204FDC,
        SecondactBigBully = 0x1204FE4
    }
}
