﻿
namespace SM64Lib.Objects
{
    public enum StarNames
    {
        KoopaTheQuick1,
        KoopaTheQuick2,
        KingBobOmbBoss,
        WhompBoss,
        EyerockBoss,
        BigBullyBoss,
        ChillBullyBoss,
        GiantPiranhaPlants,
        PenguinMother,
        BigPenguinRace,
        WigglerBoss,
        PeachSlideStar,
        TreasureChests,
        BooInHauntedHouse,
        Klepto,
        MerryGoRoundboss,
        MrIboss,
        RooftopBoo,
        SecondactBigBully
    }
}