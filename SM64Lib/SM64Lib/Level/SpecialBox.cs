﻿using global::System.IO;
using SM64Lib.Data;
using SM64Lib.Data.System;

namespace SM64Lib.Levels
{
    public class SpecialBox
    {
        public SpecialBoxType Type { get; set; } = SpecialBoxType.Water;
        public short X1 { get; set; } = 8192;
        public short Z1 { get; set; } = 8192;
        public short X2 { get; set; } = -8192;
        public short Z2 { get; set; } = -8192;
        public short Y { get; set; } = 0;
        public short Scale { get; set; } = 16;
        public bool InvisibleWater { get; set; } = false;
        public WaterType WaterType { get; set; } = WaterType.Default;
        public byte Alpha { get; set; } = 78;

        public void ToBoxData(BinaryData data)
        {
            // Stand: SM64 Editor v2.0.7

            data.Write(InvisibleWater ? 0x0 : 0x10000); // Type = SpecialBoxType.ToxicHaze OrElse
            data.Write((short)0xF);
            data.Write(Scale);
            data.Write(X1);
            data.Write(Z1);
            data.Write(X2);
            data.Write(Z1);
            data.Write(X2);
            data.Write(Z2);
            data.Write(X1);
            data.Write(Z2);

            if (Type == SpecialBoxType.ToxicHaze)
            {
                data.Write(Alpha); // &HB4
                data.Write(0x10000);
            }
            else
            {
                data.Write(0x10000 | Alpha);
                data.Write((int)WaterType);
            }
        }
    }
}